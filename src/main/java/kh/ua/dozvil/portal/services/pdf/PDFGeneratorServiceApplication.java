package kh.ua.dozvil.portal.services.pdf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PDFGeneratorServiceApplication {

    @Value("${services.pdf.files.fonts}")
    public String fontsStorage;

    public static void main(String[] args) {
        var application = new SpringApplication(PDFGeneratorServiceApplication.class);
        application.setLogStartupInfo(false);
        application.run(args);
    }
}
