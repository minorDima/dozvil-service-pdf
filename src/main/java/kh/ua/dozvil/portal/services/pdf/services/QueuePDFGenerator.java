package kh.ua.dozvil.portal.services.pdf.services;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import kh.ua.dozvil.portal.services.pdf.models.PDFQueue;
import kh.ua.dozvil.portal.services.pdf.models.PDFQueueSize;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class QueuePDFGenerator {
    private final TemplateEngine templateEngine;

    @Value("${services.pdf.files.fonts}")
    public String fontsStorage;

    public byte[] createPdf(PDFQueue pdfQueue) throws IOException {
        ByteArrayOutputStream ret = new ByteArrayOutputStream();
        var properties = new ConverterProperties();
        var fontProvider = new DefaultFontProvider();
        fontProvider.addDirectory(fontsStorage);
        properties.setFontProvider(fontProvider);
        properties.setBaseUri("classpath:/content/");

        PdfDocument pdfDocument = new PdfDocument(new PdfWriter(ret));
        if (pdfQueue.getSize() == PDFQueueSize.BIG) {
            pdfDocument.setDefaultPageSize(PageSize.A4);
        } else {
            pdfDocument.setDefaultPageSize(
                    new PageSize(2.83F * 80F, 2.83F * 180F)
            );
        }
        HtmlConverter.convertToPdf(generateDocumentHtml(pdfQueue), pdfDocument, properties);
        pdfDocument.close();
        return ret.toByteArray();
    }


    private String generateDocumentHtml(PDFQueue service) {
        Context context = new Context();
        context.setVariable("record", service);
        return this.templateEngine.process("queue.html", context);
    }
}
