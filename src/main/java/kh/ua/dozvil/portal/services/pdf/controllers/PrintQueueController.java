package kh.ua.dozvil.portal.services.pdf.controllers;

import kh.ua.dozvil.portal.services.pdf.models.PDFQueue;
import kh.ua.dozvil.portal.services.pdf.services.QueuePDFGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/queue")
public class PrintQueueController {
    private final QueuePDFGenerator generator;

    @PostMapping
    public ResponseEntity<byte[]> generatePdfForService(@RequestBody PDFQueue service) throws IOException {
        var content = generator.createPdf(service);
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.APPLICATION_PDF);
        respHeaders.setContentLength(content.length);
        respHeaders.setContentDispositionFormData("attachment", "receipt.pdf");
        return new ResponseEntity<>(content, respHeaders, HttpStatus.OK);
    }
}
