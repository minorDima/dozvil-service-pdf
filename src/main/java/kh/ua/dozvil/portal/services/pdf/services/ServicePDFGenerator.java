package kh.ua.dozvil.portal.services.pdf.services;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import kh.ua.dozvil.portal.services.pdf.models.PDFOpenOfficeService;
import kh.ua.dozvil.portal.services.pdf.models.PDFService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ServicePDFGenerator {
    private final TemplateEngine templateEngine;

    @Value("${services.pdf.files.fonts}")
    public String fontsStorage;


    public byte[] createPdf(PDFService service) throws IOException {
        ByteArrayOutputStream ret = new ByteArrayOutputStream();
        var properties = new ConverterProperties();
        properties.setBaseUri("/");
        var fontProvider = new DefaultFontProvider();
        fontProvider.addDirectory(fontsStorage);
        properties.setFontProvider(fontProvider);
        HtmlConverter.convertToPdf(generateDocumentHtml(service), ret, properties);
        return ret.toByteArray();
    }

    public byte[] createPdf(PDFOpenOfficeService service) throws IOException {
        ByteArrayOutputStream ret = new ByteArrayOutputStream();
        var properties = new ConverterProperties();
        properties.setBaseUri("/");
        var fontProvider = new DefaultFontProvider();
        fontProvider.addDirectory(fontsStorage);
        properties.setFontProvider(fontProvider);
        HtmlConverter.convertToPdf(generateDocumentHtml(service), ret, properties);
        return ret.toByteArray();
    }



    private String generateDocumentHtml(PDFService service){
        Context context = new Context();
        context.setVariable("service", service);
        return this.templateEngine.process("service.html", context);
    }

    private String generateDocumentHtml(PDFOpenOfficeService service){
        Context context = new Context();
        context.setVariable("service", service);
        return this.templateEngine.process("open-office-service.html", context);
    }
}
