package kh.ua.dozvil.portal.services.pdf.controllers;

import kh.ua.dozvil.portal.services.pdf.models.PDFOpenOfficeService;
import kh.ua.dozvil.portal.services.pdf.models.PDFService;
import kh.ua.dozvil.portal.services.pdf.services.ServicePDFGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/service")
public class PrintServiceController {
    private final ServicePDFGenerator generator;

    @PostMapping
    public ResponseEntity<byte[]> generatePdfForService(@RequestBody PDFService service) throws IOException {
        var content = generator.createPdf(service);
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.APPLICATION_PDF);
        respHeaders.setContentLength(content.length);
        respHeaders.setContentDispositionFormData("attachment", service.getLink() + ".pdf");
        return new ResponseEntity<>(content, respHeaders, HttpStatus.OK);
    }

    @PostMapping("/open-office")
    public ResponseEntity<byte[]> generatePdfForOPenOfficeService(@RequestBody PDFOpenOfficeService service) throws IOException {
        var content = generator.createPdf(service);
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.APPLICATION_PDF);
        respHeaders.setContentLength(content.length);
        respHeaders.setContentDispositionFormData("attachment", service.getId() + ".pdf");
        return new ResponseEntity<>(content, respHeaders, HttpStatus.OK);
    }
}
